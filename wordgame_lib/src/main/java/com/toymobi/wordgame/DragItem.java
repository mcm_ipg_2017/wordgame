package com.toymobi.wordgame;

import java.util.Random;

import android.content.res.Resources;
import android.graphics.drawable.Drawable;

import androidx.collection.SparseArrayCompat;
import androidx.core.content.res.ResourcesCompat;

class DragItem {

    static final CharSequence CORRECT = "correct";

    static final CharSequence INCORRECT = "incorrect";

    static final int LETTER_1 = 0;
    static final int LETTER_2 = 1;
    static final int LETTER_3 = 2;

    private static final int MAX_LETTER_SPOT = 3;

    CharSequence name;

    Drawable image;

    int lengthName;

    int indexGoalSpotUp = 0;

    int indexGoalSpotDown = 0;

    private static Random random;

    SparseArrayCompat<ItemLetter> itemsLetter;

    int letterImageCorrect = 0, letterImageIncorrectCorrect1 = 0, letterImageIncorrectCorrect2 = 0;

    DragItem(final Resources resources, final int nameId,
             final int imageId) {

        if (resources != null) {

            name = resources.getText(nameId);

            image = ResourcesCompat.getDrawable(resources, imageId, null);

            if (name != null) {

                lengthName = name.length();

                if (itemsLetter == null) {
                    itemsLetter = new SparseArrayCompat<>(lengthName);
                }

                for (int i = 0; i < lengthName; i++) {

                    final char charLetter = name.charAt(i);

                    final int imageLetterId = AlphabetStruct.getImageLetter(charLetter, resources);

                    final ItemLetter item = new ItemLetter(imageLetterId);

                    itemsLetter.put(i, item);
                }
                init();
            }
        }
    }

    final void init() {
        if (random == null) {
            random = new Random();
        }

        if (lengthName > 0 && lengthName <= WordGameView.MAX_LETTER_SPOT) {

            indexGoalSpotUp = random.nextInt(lengthName);

            indexGoalSpotDown = random.nextInt(MAX_LETTER_SPOT);


            if (itemsLetter != null && indexGoalSpotUp >= 0 && indexGoalSpotUp < itemsLetter.size()) {

                final ItemLetter item = itemsLetter.get(indexGoalSpotUp);

                if (item != null) {
                    letterImageCorrect = item.letterImageId;

                    boolean finishRandom = false;

                    while (!finishRandom) {

                        letterImageIncorrectCorrect1 =
                                AlphabetStruct.alphabet_image_id[random.nextInt(AlphabetStruct.alphabet_image_id.length)];

                        letterImageIncorrectCorrect2 =
                                AlphabetStruct.alphabet_image_id[random.nextInt(AlphabetStruct.alphabet_image_id.length)];

                        if (letterImageIncorrectCorrect1 != letterImageCorrect
                                && letterImageIncorrectCorrect2 != letterImageCorrect
                                && letterImageIncorrectCorrect1 != letterImageIncorrectCorrect2) {
                            finishRandom = true;
                        }
                    }
                }
            }
        }
    }

    final void deallocate() {

        random = null;

        name = null;

        clearParamsLetters();

        if (itemsLetter != null) {
            itemsLetter.clear();
            itemsLetter = null;
        }

        if (image != null) {
            image = null;
        }
    }

    private void clearParamsLetters() {
        lengthName = 0;

        indexGoalSpotUp = 0;

        indexGoalSpotDown = 0;

        letterImageCorrect = 0;

        letterImageIncorrectCorrect1 = 0;

        letterImageIncorrectCorrect2 = 0;
    }
}
