package com.toymobi.wordgame;

import java.util.Locale;

import com.toymobi.recursos.BaseFragment;
import com.toymobi.recursos.EduardoStuff;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.speech.tts.TextToSpeech.OnInitListener;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;

public class WordGameFragment extends BaseFragment implements OnInitListener {

    public static final String FRAGMENT_TAG_GAME_WORD = "FRAGMENT_TAG_GAME_WORD";

    private WordGameController wordGameController;

    private static final int MY_DATA_CHECK_CODE = 1234;

    @Override
    public final void onCreate(final Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        createMusicBackground(R.string.path_sound_game_menu, R.raw.minigame_sound, true);

        createSFX(R.raw.sfx_normal_click);

        createVibrationeedback();
    }

    @Override
    public final View onCreateView(@NonNull final LayoutInflater inflater,
                                   ViewGroup container,
                                   final Bundle savedInstanceState) {

        mainView = inflater.inflate(R.layout.wordgame_fragment, container, false);

        createToolBar(R.id.toolbar_word, R.string.wordgame_toolbar_name);

        if (wordGameController == null) {

            // Fire off an intent to check if a TTS engine is installed
            final Intent checkIntent = new Intent();
            checkIntent.setAction(TextToSpeech.Engine.ACTION_CHECK_TTS_DATA);
            startActivityForResult(checkIntent, MY_DATA_CHECK_CODE);

            wordGameController = new WordGameController(getActivity(), mainView);
        }
        return mainView;
    }

    @Override
    public final void onDestroy() {
        super.onDestroy();
        deallocate();
    }

    @Override
    public final void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {

        menu.clear();

        inflater.inflate(R.menu.menu_game_word, menu);

        // Set an icon in the ActionBar
        itemSound = menu.findItem(R.id.sound_menu);

        setIconSoundMenu();
    }

    @Override
    public final boolean onOptionsItemSelected(@NonNull final MenuItem item) {

        final int item_id = item.getItemId();

        playFeedBackButtons();

        if (wordGameController != null) {

            if (item_id == R.id.exit_word_game) {

                exitOrBackMenu(startSingleApp);

            } else if (item_id == R.id.change_word) {

                wordGameController.nextItemLetter();

            } else if (item_id == R.id.sound_menu) {

                changeSoundMenu();

            }
        } else {
            super.onOptionsItemSelected(item);
        }
        return true;
    }

    @Override
    public void deallocate() {

        super.deallocate();

        if (mainView != null) {
            ((ConstraintLayout) mainView).removeAllViews();
        }

        if (wordGameController != null) {
            wordGameController.deallocate();
            wordGameController = null;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == MY_DATA_CHECK_CODE && EduardoStuff.ENABLE_TTS) {

            if (resultCode == TextToSpeech.Engine.CHECK_VOICE_DATA_PASS) {

                final Context context = getContext();

                if (context != null) {
                    wordGameController.textToSpeech = new TextToSpeech(context, WordGameFragment.this);
                    // bookSPCController.setTTS();
                }
            } else {
                final Intent installIntent = new Intent();
                installIntent.setAction(TextToSpeech.Engine.ACTION_INSTALL_TTS_DATA);
                startActivity(installIntent);
            }
        }
    }

    @Override
    public void onInit(int status) {
        if (EduardoStuff.ENABLE_TTS && status == TextToSpeech.SUCCESS && wordGameController != null) {

            final Locale locateBR = new Locale("pt_BR");

            final int result = wordGameController.textToSpeech
                    .setLanguage(locateBR);

            final Locale locatePT = new Locale("pt");

            final int result_2 = wordGameController.textToSpeech
                    .setLanguage(locatePT);

            if ((result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_NOT_SUPPORTED)
                    || (result_2 == TextToSpeech.LANG_MISSING_DATA || result_2 == TextToSpeech.LANG_NOT_SUPPORTED)) {
                EduardoStuff.ENABLE_TTS = false;
            } else {
                wordGameController.setTTS();
            }
        } else {
            EduardoStuff.ENABLE_TTS = false;
        }
    }
}
