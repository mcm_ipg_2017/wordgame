package com.toymobi.wordgame;

interface ItemStruct {

    int[] item_text_id = {
            R.string.item_1_text,
            R.string.item_2_text,
            R.string.item_3_text,
            R.string.item_4_text,
            R.string.item_5_text,
            R.string.item_6_text,
            R.string.item_7_text,
            R.string.item_8_text};

    int[] item_image_id = {
            R.drawable.item_1,
            R.drawable.item_2,
            R.drawable.item_3,
            R.drawable.item_4,
            R.drawable.item_5,
            R.drawable.item_6,
            R.drawable.item_7,
            R.drawable.item_8};

    int ITEM_SIZE = item_text_id.length;

}
