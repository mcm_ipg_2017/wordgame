package com.toymobi.wordgame;

import android.content.Context;
import android.content.res.Resources;
import android.os.Handler;
import android.speech.tts.TextToSpeech;
import android.view.View;

import androidx.collection.SparseArrayCompat;

import com.toymobi.framework.feedback.VibrationFeedback;
import com.toymobi.framework.media.SimpleSoundPool;
import com.toymobi.framework.view.imageview.ImageViewAnimationTTS;
import com.toymobi.recursos.EduardoStuff;

class WordGameController {

    private WordGameView wordGameView;

    private ImageViewAnimationTTS itemImage;

    private OnNextItemListener onNextItemListener;

    private VibrationFeedback vibrationFeedback;

    private SimpleSoundPool sfx;

    private Resources resources;

    private SparseArrayCompat<DragItem> dragItems;

    private static int itemIndex = 0;

    private Handler nextItemHandler;

    TextToSpeech textToSpeech;

    WordGameController(final Context context, final View rootViewLayout) {

        if (context != null) {

            resources = context.getResources();

            if (vibrationFeedback == null) {
                vibrationFeedback = new VibrationFeedback(context);
            }

            if (sfx == null) {
                sfx = new SimpleSoundPool(context, R.raw.sfx_normal_click, R.raw.sfx_winner, R.raw.sfx_lose);
            }

            if (rootViewLayout != null) {
                wordGameView = rootViewLayout.findViewById(R.id.word_view_layout_container);
                itemImage = rootViewLayout.findViewById(R.id.item_image);
            }

            if (dragItems == null) {
                dragItems = new SparseArrayCompat<>(ItemStruct.ITEM_SIZE);
            }

            startItemLetter();
        }
    }

    final void deallocate() {

        if (nextItemHandler != null) {
            nextItemHandler = null;
        }
        stopTTS();

        if (sfx != null) {
            sfx.release();
            sfx = null;
        }

        if (wordGameView != null) {
            wordGameView.deallocate();
            wordGameView = null;
        }

        for (int i = 0; i < ItemStruct.ITEM_SIZE; i++) {
            DragItem dragItem = dragItems.get(i);

            if (dragItem != null) {
                dragItem.deallocate();
            }
        }
        dragItems.clear();
        dragItems = null;
    }

    private void startItemLetter() {

        createItemLetters();

        nextItemLetter();
    }

    final void nextItemLetter() {

        if (wordGameView != null) {

            if (itemIndex < ItemStruct.ITEM_SIZE - 1) {
                itemIndex++;
            } else {
                itemIndex = 0;
            }

            wordGameView.eraserSpot();

            wordGameView.setDragItem(dragItems.get(itemIndex));

            itemImage.setImageDrawable(null);

            final DragItem item = dragItems.get(itemIndex);

            if (item != null) {
                itemImage.setImageDrawable(item.image);

                itemImage.setText(item.name.toString());

                wordGameView.start();
            }
        }
    }

    private void createItemLetters() {
        if (dragItems != null && dragItems.size() == 0) {

            for (int i = 0; i < ItemStruct.ITEM_SIZE; i++) {
                final DragItem dragItem = new DragItem(resources, ItemStruct.item_text_id[i], ItemStruct.item_image_id[i]);
                dragItems.put(i, dragItem);
            }
        }

        if (dragItems != null) {

            final int n = dragItems.size();

            for (int i = 0; i < dragItems.size(); i++) {

                // Get a random index of the array past i.
                final int random = i + (int) (Math.random() * (n - i));

                // Swap the random element with the present element.
                final DragItem randomElement = dragItems.get(random);
                dragItems.append(random, dragItems.get(i));
                dragItems.append(i, randomElement);
            }
        }

        if (onNextItemListener == null) {
            onNextItemListener = new OnNextItemListener() {

                @Override
                public void nextItem() {
                    if (sfx != null) {
                        sfx.playSound(R.raw.sfx_winner);
                    }

                    if (nextItemHandler == null) {
                        nextItemHandler = new Handler();
                    }

                    nextItemHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            nextItemLetter();
                        }
                    });
                }

                @Override
                public void wrongItem() {
                    if (sfx != null) {
                        sfx.playSound(R.raw.sfx_lose);
                    }
                }
            };
            wordGameView.onNextItemListener = onNextItemListener;
        }
    }

    private void stopTTS() {
        if (EduardoStuff.ENABLE_TTS && textToSpeech != null) {
            textToSpeech.stop();
            textToSpeech.shutdown();
        }
    }

    final void setTTS() {
        if (itemImage != null) {
            itemImage.setTextToSpeech(textToSpeech);
        }
    }
}
