package com.toymobi.wordgame;

import android.content.res.Resources;
import android.util.SparseIntArray;

class AlphabetStruct {

    private static SparseIntArray letterStruct = null;

    private static final int[] alphabet_letter_id = {
            R.string.a_letter,
            R.string.b_letter,
            R.string.c_letter,
            R.string.d_letter,
            R.string.e_letter,
            R.string.f_letter,
            R.string.g_letter,
            R.string.h_letter,
            R.string.i_letter,
            R.string.j_letter,
            R.string.l_letter,
            R.string.m_letter,
            R.string.n_letter,
            R.string.o_letter,
            R.string.p_letter,
            R.string.q_letter,
            R.string.r_letter,
            R.string.s_letter,
            R.string.t_letter,
            R.string.u_letter,
            R.string.v_letter,
            R.string.x_letter,
            R.string.z_letter,
            R.string.an_letter};

    static final int[] alphabet_image_id = {R.drawable.image_letter_a,
            R.drawable.image_letter_b, R.drawable.image_letter_c,
            R.drawable.image_letter_d, R.drawable.image_letter_e,
            R.drawable.image_letter_f, R.drawable.image_letter_g,
            R.drawable.image_letter_h, R.drawable.image_letter_i,
            R.drawable.image_letter_j, R.drawable.image_letter_l,
            R.drawable.image_letter_m, R.drawable.image_letter_n,
            R.drawable.image_letter_o, R.drawable.image_letter_p,
            R.drawable.image_letter_q, R.drawable.image_letter_r,
            R.drawable.image_letter_s, R.drawable.image_letter_t,
            R.drawable.image_letter_u, R.drawable.image_letter_v,
            R.drawable.image_letter_x, R.drawable.image_letter_z,
            R.drawable.image_letter_an};

    static int getImageLetter(final char letter,
                              final Resources resources) {

        int index;

        if (letterStruct == null) {

            final int length = alphabet_image_id.length;

            letterStruct = new SparseIntArray(length);

            SparseIntArray alphabet = new SparseIntArray(length);

            for (int i = 0; i < length; i++) {

                char letterChar = resources.getText(alphabet_letter_id[i])
                        .charAt(0);

                letterStruct.put(letterChar, alphabet_image_id[i]);

                alphabet.put(i, letterChar);
            }
        }
        index = letterStruct.get(letter);

        return index;
    }
}
