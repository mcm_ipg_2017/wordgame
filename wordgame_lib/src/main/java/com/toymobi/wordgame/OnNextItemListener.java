package com.toymobi.wordgame;

interface OnNextItemListener {

    void nextItem();

    void wrongItem();

}
