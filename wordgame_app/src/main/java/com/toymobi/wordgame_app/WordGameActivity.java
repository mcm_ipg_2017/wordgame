package com.toymobi.wordgame_app;

import android.os.Bundle;
import android.view.WindowManager;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.toymobi.recursos.BaseFragment;
import com.toymobi.wordgame.WordGameFragment;

public class WordGameActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.wordgame_activity);
        startFragment();
    }

    private void startFragment() {

        WordGameFragment.startSingleApp = true;

        final FragmentManager fragmentManager = getSupportFragmentManager();

        final FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        final BaseFragment wordGameFragment = new WordGameFragment();

        fragmentTransaction.add(R.id.fragment_container,
                wordGameFragment,
                WordGameFragment.FRAGMENT_TAG_GAME_WORD);

        fragmentTransaction.commit();
    }
}
